- TODO: add option to repeat last timer, maybe for x times
- TODO: fullscreenmode for presentation timing

v2.1.0
- add Norwegian translation, thanks to JanC Sherdahl
- add Dutch translation, thanks to Heimen Stoffels
- add option for "no sound" timers
- refactor about page (add links to source and changelog, delete some broken links, internal redesign, remove dividers)
- replace all hardcoded colors with themed colors (one exception kept)
- fix system sound list not displaying the last few entries
- removed dividers in several places
- reformat some strings to allow better translations by using .arg()
- update framework to 16.04.4

v2.0.5
- move tick icon for all settings to the right side
- temporary remove link for translate-ut.org
- remove link to SwipeToBack poll
- fix swipeDistance not showing default value on first start
- fix SwipeToBack not working on timer details page

v.2.0.4
- translation now available via community weblate on translate-ut.org
- redesign of SwipeToBack
  * SwipeToBack now always works in both directions (left and right)
  * back click/tap and SwipeToBack are both enabled to allow convergent use with touch and mouse
  * use swipetoback icon instead of back icon (can be clicked/tapped as before)
- updated translations, thanks to all translators

v2.0.3
- updated spanish translation, thanks Krakakanok
- small changes to first time notes on adding favourites
- all setting strings now start with capital letter

v2.0.2
- about page now only flickable if needed

v2.0.1
- updated french translation, thanks Anne Onyme 017

v2.0.0
- add swipe to action feature
- respect system theme (now under settings) and adjust colours to be set according to theme
- add system ringtones
- add option for close button on main page
- partially redesign of about page
- implement clickable kill command

v1.3.2
- buttons now always visible

v1.3.1
- small translation updates
- change version to 3 digits to allow semantic versioning

v1.3
- add important information to about page
- add french translation thanks to Anne Onyme 017
- add spanish translation, thanks to Krakakanok
- increased button size

v1.2
- new maintainer danfro
- bumped version to 1.2
- moved source code from launchpad to gitlab
- allow timers up to 24 hours

v1.1
- build for 16.04 framework
- last version based on launchpad code

v1.0
- allow multiple timers
